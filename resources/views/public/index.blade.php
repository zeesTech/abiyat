<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
        <!-- Font awesome -->
        <script src="https://kit.fontawesome.com/d15b038ad9.js" crossorigin="anonymous"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Custome style sheet -->
        <link rel="stylesheet"  href="public_assets/css/customstyle.css" type="text/css">
        <title>noon</title>
    </head>

    
    <body>
        <!-- Menue Start -->
        <nav class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color:#005E00;">
            <!-- This displays a phone button on mobile only -->
              <div class="navbar-toggler-right">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
               </div>
               
              <div class=" collapse navbar-collapse flex-column " id="navbar">
                  <ul class=" navbar-nav bg-transparent justify-content-center w-100  px-3">
                      <li class="nav-item active">
                          <a class="nav-link border-right top-menu-bar" style="color:white; font-size: 12px; font-family: lato;font-family: 'Lato', sans-serif;"href="#">العربية <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item dropdown">
                          <a class="nav-link text-decoration-none " style="color:white; font-size: 12px; font-family: lato;font-family: 'Lato', sans-serif;" href="#"  role="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fab fa-font-awesome-flag"></i>
                              <span>Ship to</span>
                              <span>UAE</span>
                              <i class="fas fa-caret-down "></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                              <a class="dropdown-item"  href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                       </li>
                      <li class="nav-item active pl-5 ">
                          <a href="#">
                            <span class="text-right "style="color:white; font-size: 12px; font-family: lato;font-family: 'Lato', sans-serif;"><i class="fas fa-sync-alt pr-2"></i>FREE & EASY RETURNS</span>
                          </a>
                      </li>
                      <li class="nav-item active pl-5 ">
                            <a href="#">
                                <span class="text-right"style="color:white; font-size: 12px; font-family: lato;font-family: 'Lato', sans-serif;"><i class="fas fa-gift pr-2"></i>BEST DEALS</span>
                            </a>
                      </li>
                  </ul>            
                  <ul class="navbar-nav  w-100 bg-transparent justify-content-center px-3">
                      <li class="nav-item  justify-content-start active mr-5 w-10">
                          <a href="#" class="bg-white"> 
                              <img src="public_assets/images/logo.png" style="height:100%;width:93px;" alt="">
                          </a>
                      </li>
                      <li class="nav-item active w-50">
                          <div class="input-group" >
                              <input type="text" class="form-control" placeholder="What are you looking for?" aria-describedby="basic-addon2">
                              <div class="input-group-append">
                                <button class="btn btn-outline-light top-menu-bar2"type="button">Search</button>
                              </div>
                          </div>
                       </li>
                      <li class="nav-item dropdown active w-20">
                          <div class="border-right border-secondary">
                              <a class="nav-link text-decoration-none text-white top-menu-bar2" href="#"  role="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <span>Sign in or sign up</span>
                                  <i class="fas fa-caret-down "></i>
                                </a>
                                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                      <a class="dropdown-item" href="#">Action</a>
                                      <a class="dropdown-item" href="#">Another action</a>
                                      <a class="dropdown-item" href="#">Something else here</a>
                                  </div>
                          </div>
                      </li>
                      <li class="nav-item active w-20">
                          <div class="pl-3 pr-3">
                              <a class="nav-link text-decoration-none text-white top-menu-bar2 " href="#"  role="">
                                  <span>Cart</span>
                                  <i class="fas fa-shopping-cart"></i>
                              </a>  
                          </div>
                      </li>
                  </ul>
                  <ul class="navbar-nav justify-content-center bg-light w-100 px-3">
                      <a class="navbar-brand border-right text-dark pr-3 top-menu-bar3" href="#">Tech Technologies <i class="fas fa-caret-down"></i></a>
                      <li class="nav-item">
                          <a class="nav-link text-dark top-menu-bar3"  href="#">ELECTRONICS</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-dark top-menu-bar3" href="#">FASHION</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link  text-dark top-menu-bar3" href="#">HOME</a>
                      </li>
                      <li class="nav-item dropdown menu-area">
                          <a class="nav-link ml-1 mr-1 text-dark top-menu-bar3" href="#" id="mega-two"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          BEAUTY & FRAGRANCE
                          </a>
                          <div class="dropdown-menu mega-area" aria-labelledby="mega-two">
                          <div class="row">
                          <div class="col-6">
                              <div class="row">
                                  <div class="col-lg-4">
                                      <p><b> Catagory</b></p>
                                      <a href="#" class="dropdown-item">Item 1</a>
                                      <a href="#" class="dropdown-item">Item 2</a>
                                      <a href="#" class="dropdown-item">Item 3</a>
                                      <a href="#" class="dropdown-item">Item 4</a>
                                      <a href="#" class="dropdown-item">Item 5</a>
                                      <a href="#" class="dropdown-item">Item 6</a>
                                      <a href="#" class="dropdown-item">Item 7</a> 
                                  </div>
                                  <div class="col-lg-8">
                                  <p><b> Graph Design</b></p>
                                  <div class="row">
                                      <div class="col-4  p-1 ">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>       
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>    
                                  </div>
                                  <div class="row">
                                      <div class="col-4  p-1 ">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>       
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>    
                                  </div>
                                  <div class="row">
                                      <div class="col-4  p-1 ">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>       
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>    
                                  </div>
                                  </div> 
                              </div>
                          </div> 
                          <div class="col-6">
                              <div class="row">
                                  <div class="col-lg-8" >
                                      <a href="#" class="dropdown-item"><img src="public_assets/images/mega image.jpg" class="img-fluid" style="height: 310px;" alt=""></a>
                                  </div>
                                  <div class="col-lg-4" >
                                      <a href="#" class="dropdown-item" ><img src="public_assets/images/mega image1.jpg"class=" img-fluid" style="height: 310px;" alt=""></a> 
                                  </div>
                              </div>
                          </div>
                          </div>
                          </div>
                      </li>
                      <li class="nav-item dropdown menu-area">
                          <a class="nav-link ml-1 mr-1 text-dark top-menu-bar3" href="#" id="mega-two"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          BABY & TOYS
                          </a>
                          <div class="dropdown-menu mega-area" aria-labelledby="mega-two">
                          <div class="row">
                          <div class="col-6">
                              <div class="row">
                                  <div class="col-lg-4">
                                      <p><b> Catagory</b></p>
                                      <a href="#" class="dropdown-item">Item 1</a>
                                      <a href="#" class="dropdown-item">Item 2</a>
                                      <a href="#" class="dropdown-item">Item 3</a>
                                      <a href="#" class="dropdown-item">Item 4</a>
                                      <a href="#" class="dropdown-item">Item 5</a>
                                      <a href="#" class="dropdown-item">Item 6</a>
                                      <a href="#" class="dropdown-item">Item 7</a> 
                                  </div>
                                  <div class="col-lg-8">
                                  <p><b> Graph Design</b></p>
                                  <div class="row">
                                      <div class="col-4  p-1 ">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>       
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>    
                                  </div>
                                  <div class="row">
                                      <div class="col-4  p-1 ">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>       
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>    
                                  </div>
                                  <div class="row">
                                      <div class="col-4  p-1 ">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>       
                                      </div>
                                      <div class="col-4 p-1">
                                          <a href="#" class="dropdown-item"><img src="public_assets/images/mega image item.png"class="border shadow-sm img-fluid" alt=""></a>
                                      </div>    
                                  </div>
                                  </div> 
                              </div>
                          </div> 
                          <div class="col-6">
                              <div class="row">
                                  <div class="col-lg-8" >
                                      <a href="#" class="dropdown-item"><img src="public_assets/images/mega image.jpg" class="img-fluid" style="height: 310px;" alt=""></a>
                                  </div>
                                  <div class="col-lg-4" >
                                      <a href="#" class="dropdown-item" ><img src="public_assets/images/mega image1.jpg"class=" img-fluid" style="height: 310px;" alt=""></a> 
                                  </div>
                              </div>
                          </div>
                          </div>
                          </div>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-dark top-menu-bar3" href="#">GROCERY</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-dark top-menu-bar3" href="#">SPORTS</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-dark top-menu-bar3" href="#">THE DUBAI MALL STORE</a>
                      </li>
                  </ul>
            </div>
          </nav>
        <!-- Menu Close -->


        <div class="container-fluid p-0 bg-light">
            <!--Carousel Open  -->
            <div class="container-fluid p-0 bg-light">
        
                <div id="carouselExampleIndicators" class="carousel slide bg-light " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=" indicator-color active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="indicator-color"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="indicator-color"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3" class="indicator-color"></li>
                    </ol>
                    <div class="carousel-inner ">
                    <div class="carousel-item active ">
                        <a href="#"><img src="public_assets/images/slider.png" class="d-block w-100 " alt="..."></a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/slider.png" class="d-block w-100" alt="...">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/slider.png" class="d-block w-100" alt="...">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/slider.png" class="d-block w-100" alt="...">
                        </a>
                    </div>
                    </div>
                    <a class="carousel-control-prev " href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next " href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon " aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            
            </div>
            <!--Carousel Close -->


           <!-- Circle Images open-->
           <div class="container-fluid pt-4 bg-white">
            <div class="row pl-5 pr-5">
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/APPLIANCES.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>APPLIANCES</b></span>
                        </a> 
                    </div>
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/BABY ESSENTIAL.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>BABY ESSENTIAL</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/BEAUTY BOUTIQUE.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>BEAUTY BOUTIQUE</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/BEAUTY.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>BEAUTY</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/ELECTRONICS.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>ELECTRONICS</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/FASHION.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b> FASHION</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/FRAGRANCES.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>FRAGRANCES</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/GROCERIES.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>GROCERIES</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/KICTHEN.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>KICTHEN</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/MOBILES.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>MOBILES</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/SPORTS.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>SPORTS</b></span>
                        </a> 
                    </div> 
                </div>
                <div class="col-12 col-sm-3 col-md-2 col-lg-1 p-1 text-center ">
                    <div class="m-1 bg-white">
                        <a href="#" class="text-decoration-non text-dark ">
                            <img src="public_assets/images/circle/TOYS & GAMES.png" class="img-fluid" alt="">
                            <br>
                            <span class="circle-image-caption "><b>TOYS & GAMES</b></span>
                        </a> 
                    </div> 
                </div>
            </div>
            </div>
            <!-- Cicle Images closed -->


            <!-- Product Carousel open-->
            <!--Carousel Open  -->
            <div class="container-fluid bg-light">
                <div class="pt-5">
                    <h5><b>Recommended For You</b></h5>
                </div>
            <div class="row">
                <div class="col">
                    <div id="carouselProductIndicators1" class="carousel slide  " data-ride="carousel">
                        <div class="carousel-inner carousel-inner-product justify-content-center">
                            <!-- carousel item 1 open -->
                            <div class="carousel-item active ">
                                <!-- product Carousel -->
                                <div class="container-fluid p-0 bg-light">
                                    <div class="row ">
                                        <!-- Product 1 Open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 1 close -->
                                        <!-- Product 2 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 2 close -->
                                        <!-- Product 3 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 3 close -->
                                        <!-- Product 4 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 4 close -->
                                        <!-- Product 5 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 5 close -->
                                        <!-- Product 6 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 6 close -->
                                    </div>
                                </div>
                                <!-- Product carousel close -->
                            </div>
                            <!-- carousel item 1 close -->
                            <!-- carousel item 2 open -->
                            <div class="carousel-item ">
                                <!-- product Carousel -->
                                <div class="container-fluid p-0 bg-light">
                                    <div class="row ">
                                        <!-- Product 1 Open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >10% use aouto filled this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 1 close -->
                                        <!-- Product 2 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 2 close -->
                                        <!-- Product 3 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 3 close -->
                                        <!-- Product 4 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 4 close -->
                                        <!-- Product 5 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 5 close -->
                                        <!-- Product 6 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 6 close -->
                                    </div>
                                </div>
                                <!-- Product carousel close -->
                            </div>
                            <!-- carousel item 1 close -->
                        </div>
                        <a class="carousel-control-prev" href="#carouselProductIndicators1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselProductIndicators1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        
                        </div>
                </div>
            </div>

            </div>
            <!-- Product Carousel close-->
            
            
            <!-- Add Rectangle Giff Open-->
            <div class="container-fluid bg-light">
                <a href="#">
                    <img src="public_assets/images/add rectangle.gif" class="img-fluid"  alt="">
                </a>
            </div>
            <!-- Add Rectangle Giff Close-->

            <!-- Add Recatngle Giff Small Open-->
            <div class="container-fluid p-0 mt-5 mb-5">
                <a href="#">
                    <img src="public_assets/images/add rectangle small.gif" class="img-fluid"  alt="">
                </a>
            </div>
            <!-- Add Recatngle Giff Small Close-->

            <!-- Add with Code Open-->
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-4 ">
                        <a href="#">
                            <img src="public_assets/images/add code.jpg" class="img-fluid"  alt="">
                        </a>
                    </div>
                    <div class="col-4 ">
                        <a href="#">
                            <img src="public_assets/images/add code.jpg" class="img-fluid"  alt="">
                        </a>
                    </div>
                    <div class="col-4 ">
                        <a href="#">
                            <img src="public_assets/images/add code.jpg" class="img-fluid"  alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!-- Add with Code Close-->


            <!-- Product Carousel open-->
            <!--Carousel Open  -->
            <div class="container-fluid bg-light">
                <div class="pt-5">
                    <h5><b>Recommended For You</b></h5>
                </div>
            <div class="row">
                <div class="col">
                    <div id="carouselProductIndicators1" class="carousel slide  " data-ride="carousel">
                        <div class="carousel-inner carousel-inner-product justify-content-center">
                            <!-- carousel item 1 open -->
                            <div class="carousel-item active ">
                                <!-- product Carousel -->
                                <div class="container-fluid p-0 bg-light">
                                    <div class="row ">
                                        <!-- Product 1 Open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 1 close -->
                                        <!-- Product 2 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 2 close -->
                                        <!-- Product 3 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 3 close -->
                                        <!-- Product 4 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 4 close -->
                                        <!-- Product 5 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 5 close -->
                                        <!-- Product 6 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 6 close -->
                                    </div>
                                </div>
                                <!-- Product carousel close -->
                            </div>
                            <!-- carousel item 1 close -->
                            <!-- carousel item 2 open -->
                            <div class="carousel-item ">
                                <!-- product Carousel -->
                                <div class="container-fluid p-0 bg-light">
                                    <div class="row ">
                                        <!-- Product 1 Open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >10% use aouto filled this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 1 close -->
                                        <!-- Product 2 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 2 close -->
                                        <!-- Product 3 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 3 close -->
                                        <!-- Product 4 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 4 close -->
                                        <!-- Product 5 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 5 close -->
                                        <!-- Product 6 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 6 close -->
                                    </div>
                                </div>
                                <!-- Product carousel close -->
                            </div>
                            <!-- carousel item 1 close -->
                        </div>
                        <a class="carousel-control-prev" href="#carouselProductIndicators1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselProductIndicators1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        
                        </div>
                </div>
            </div>

            </div>
            <!-- Product Carousel close-->


            <!-- Add Mega Deal Open -->
            <div class="container-fluid ">
                <div class="row p-2">
                    <div>
                        <img src="public_assets/images/mega deal.png" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="row p-2">
                    <div class="col-3 ">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-3 ">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-3 ">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-3 ">
                        <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            <!-- Add Mega Deal Open -->

            <!--Carousel Open  -->
            <div class="container-fluid p-0 mt-5 bg-light">
        
                <div id="carouselExampleIndicators1" class="carousel slide bg-light " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner ">
                        <div class="carousel-item active ">
                            <a href="#"><img src="public_assets/images/Banner 1.png" class="d-block w-100 " alt="..."></a>
                        </div>
                        <div class="carousel-item">
                            <a href="#">
                                <img src="public_assets/images/Banner 1.png" class="d-block w-100" alt="...">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="#">
                                <img src="public_assets/images/Banner 1.png" class="d-block w-100" alt="...">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="#">
                                <img src="public_assets/images/Banner 1.png" class="d-block w-100" alt="...">
                            </a>
                        </div>
                    </div>
                    <a class="carousel-control-prev " href="#carouselExampleIndicators1" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next " href="#carouselExampleIndicators1" role="button" data-slide="next">
                    <span class="carousel-control-next-icon " aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!--Carousel Close -->

           <!-- Product Carousel open-->
            <!--Carousel Open  -->
            <div class="container-fluid bg-light">
                <div class="pt-5">
                    <h5><b>Recommended For You</b></h5>
                </div>
            <div class="row">
                <div class="col">
                    <div id="carouselProductIndicators1" class="carousel slide  " data-ride="carousel">
                        <div class="carousel-inner carousel-inner-product justify-content-center">
                            <!-- carousel item 1 open -->
                            <div class="carousel-item active ">
                                <!-- product Carousel -->
                                <div class="container-fluid p-0 bg-light">
                                    <div class="row ">
                                        <!-- Product 1 Open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 1 close -->
                                        <!-- Product 2 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 2 close -->
                                        <!-- Product 3 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 3 close -->
                                        <!-- Product 4 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 4 close -->
                                        <!-- Product 5 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 5 close -->
                                        <!-- Product 6 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 6 close -->
                                    </div>
                                </div>
                                <!-- Product carousel close -->
                            </div>
                            <!-- carousel item 1 close -->
                            <!-- carousel item 2 open -->
                            <div class="carousel-item ">
                                <!-- product Carousel -->
                                <div class="container-fluid p-0 bg-light">
                                    <div class="row ">
                                        <!-- Product 1 Open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >10% use aouto filled this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 1 close -->
                                        <!-- Product 2 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 2 close -->
                                        <!-- Product 3 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 3 close -->
                                        <!-- Product 4 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 4 close -->
                                        <!-- Product 5 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 5 close -->
                                        <!-- Product 6 open -->
                                        <div class="col-sm-6 col-md-3 col-lg-2 p-2">
                                            <div style="width:200px;height:fit-content">
                                                <a href="#" class="text-decoration-non text-dark p-0">
                                                    <div class="bg-primary p-0">
                                                        <span class="pl-2 pr-2 top-discount-bar" >this is tag area</span>
                                                    </div>    
                                                    <div class="p-0">
                                                        <img src="public_assets/images/product1.jpg" class="img-fluid" alt="">
                                                    </div >
                                                    <div class="pl-2 pr-2">
                                                        <span class="product-detail product-description" >this is product detail and it is show below</span>
                                                    </div>
                                                    <div class="from-inline pl-2 pr-2">
                                                        <span class="text-muted currency-name" >AED</span>
                                                        <span class="currecny-value"><b>19.50</b></span>
                                                        <span class="currency-line">AED</span>
                                                        <span class="value-line">57</span>
                                                    </div>
                                                    <div class="form-inline pl-2 p-0">
                                                        <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                                                        <span class="bg-light pl-2 off-value" > 27% OFF</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Product 6 close -->
                                    </div>
                                </div>
                                <!-- Product carousel close -->
                            </div>
                            <!-- carousel item 1 close -->
                        </div>
                        <a class="carousel-control-prev" href="#carouselProductIndicators1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselProductIndicators1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        
                        </div>
                </div>
            </div>

            </div>
            <!-- Product Carousel close-->

            <!-- Featured Brand open-->
            <div class="container-fluid bg-white p-5">
                <div class="d-flex justify-content-between">
                    <div class="">
                        <h5><b>FEATURED BRAND</b></h5>
                    </div>
                    <div class="">
                        <button type="button" class="btn btn-light btn-sm border">View All</button>
                    </div>
                </div>
                <div class="row bg-transparent">
                    <div class="col p-2 ">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>       
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>    
                </div>
                <div class="row bg-transparent">
                    <div class="col p-2 ">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>       
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>
                    <div class="col p-2">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/FEATURED BRAND.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>    
                </div>
            </div>
            <!-- Featured Brand close-->
            
            <!-- open-->
            <div class="container-fluid bg-white p-5">
                <div class="row bg-transparent">
                    <div class="col p-2 ">
                        <a href="#" class="dropdown-item"><img src="public_assets/images/Banner 2.png"class="border shadow-sm img-fluid" alt=""></a>
                    </div>   
                </div>
                <div class="row bg-transparent">
                    <div class="col-xm-6 col-md-3 col-sm-3 p-2 ">
                            <a href="#" class="text-center mt-2 dropdown-item"><img src="public_assets/images/PRODUCT 1.png"class="border shadow-sm img-fluid" alt="">
                                <h5><b>MOBILES</b></h5>
                            </a>
                    </div>
                    <div class="col-xm-6 col-md-3 col-sm-3 p-2">
                        <a href="#" class="text-center mt-2 dropdown-item"><img src="public_assets/images/PRODUCT 2.png"class="border shadow-sm img-fluid" alt="">
                            <h5><b>SPORTS</b></h5>
                        </a>       
                    </div>
                    <div class="col-xm-6 col-md-3 col-sm-3 p-2">
                        <a href="#" class="text-center mt-2 dropdown-item"><img src="public_assets/images/PRODUCT 3.png"class="border shadow-sm img-fluid" alt="">
                            <h5><b>KITCHEN ELECTRONICS</b></h5>
                        </a>
                    </div>
                    <div class="col-xm-6 col-md-3 col-sm-3 p-2">
                        <a href="#" class="text-center mt-2 dropdown-item"><img src="public_assets/images/PRODUCT 4.png"class="border shadow-sm img-fluid" alt="">
                            <h5><b>GROSERIES</b></h5>
                        </a>
                    </div>   
                </div>
            </div>
            <!-- opne-->





        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>