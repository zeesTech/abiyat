@extends('admin.layout.app')
@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Products</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Products</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-8">
          <div class="card card-default">
            <div class="card-header">
              <h3 class="card-title">Add Product</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                      <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">Product Title</label>
                          <div class="col-sm-10">
                          <input type="text" class="form-control" id="" placeholder="">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">Product Description</label>
                          <div class="col-sm-10">
                          <textarea  class="form-control" id="" placeholder=""></textarea>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">Short Description</label>
                          <div class="col-sm-10">
                          <textarea  class="form-control" id="" placeholder=""></textarea>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">Regular Price</label>
                          <div class="col-sm-6">
                          <input type="text" class="form-control" id="" placeholder="">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">Sale Price</label>
                          <div class="col-sm-6">
                          <input type="text" class="form-control" id="" placeholder="">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">Stock Status</label>
                          <div class="col-sm-6">
                          <select class="form-control">
                            <option>In stock</option>
                            <option>Out of stock</option>
                            <option>On backorder</option>
                          </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">Is Featured</label>
                          <div class="col-sm-6">
                          <select class="form-control">
                            <option>yes</option>
                            <option>no</option>
                            
                          </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">Flash Sale</label>
                          <div class="col-sm-6">
                          <select class="form-control">
                            <option>yes</option>
                            <option>no</option>
                          </select>
                          </div>
                      </div>
                </div>
              </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            <!-- /.card-body -->
          </div>
        </div>

        <div class="col-md-4">

          <!-- /.card -->
            <!-- general form elements disabled -->
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Go Live</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                      
                      </div> 
                    </div> 
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
              <div class="card-footer text-right">
                  <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </div>
            <!-- /.card -->

            <!-- general form elements disabled -->
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Product Categories</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-12">
                      <!-- checkbox -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Checkbox</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" checked="">
                          <label class="form-check-label">Checkbox checked</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" disabled="">
                          <label class="form-check-label">Checkbox disabled</label>
                        </div>
                      </div>
                    </div> 
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- general form elements disabled -->
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Brands</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Checkbox</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" checked="">
                          <label class="form-check-label">Checkbox checked</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" disabled="">
                          <label class="form-check-label">Checkbox disabled</label>
                        </div>
                      </div> 
                    </div> 
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

             <!-- /.card -->
            <!-- general form elements disabled -->
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Product Image</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                      <input type="file" class="" id="">
                      </div> 
                    </div> 
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

               <!-- /.card -->
            <!-- general form elements disabled -->
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Product Gallery</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form">
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                      <input type="file" class="" id="">
                      </div> 
                    </div> 
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>


        <!-- /.card -->
      </div>


      
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @endsection