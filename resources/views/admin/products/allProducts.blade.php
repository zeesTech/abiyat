@extends('admin.layout.app')
)
@section('stylesheets')
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="../bower_components/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../bower_components/admin-lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<style>
.table-avatar{
    border-radius: 25%;
display: inline;
width: 2.5rem;
}
</style>
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Products</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Products</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Products</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Stock</th>
                <th>Price</th>
                <th>Categories</th>
                <th>Date</th>

              </tr>
              </thead>
              <tbody>
              <tr>
                <td><img alt="Avatar" class="table-avatar" src="https://brandfactory.pk/wp-content/uploads/2020/09/29-280x280.jpg"></td>
                <td>Z.A.R.A – Men Slim Fit Shirt ZM023
                </td>
                <td>In stock (4)</td>
                <td>1,125.00</td>
                <td>Trending, Mens, Mens Casual shirts</td>
                <td>Published
                  <br>2020/09/29</td>
                </tr>
              </tbody>
              <tfoot>
              <tr>
                  <th>Image</th>
                <th>Name</th>
                <th>Stock</th>
                <th>Price</th>
                <th>Categories</th>
                <th>Date</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
@push('scripts')
<!-- DataTables -->
<script src="../bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../bower_components/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../bower_components/admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../bower_components/admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>


<!-- page script -->
<script>
$(function () {
$("#example1").DataTable({
  "responsive": true,
  "autoWidth": false,
});
});
</script>
@endpush